import com.pajato.ctci.Node
import com.pajato.ctci.createLinkedList
import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `verify empty lists generate an empty list`() {
        assertEquals(null, sumLists(null, null))
    }

    @Test fun `verify one empty list returns the other non-empty list`() {
        val list1: Node? = null
        val list2 = Node(1)
        val expected = Node(1)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify 1 plus 1 returns 2`() {
        val list1 = Node(1)
        val list2 = Node(1)
        val expected = Node(2)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify 9 plus 9 returns 18`() {
        val list1 = Node(9)
        val list2 = Node(9)
        val expected = createLinkedList(1, 8)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify 9 plus 234 returns 243`() {
        val list1 = Node(9)
        val list2 = createLinkedList(2, 3, 4)
        val expected = createLinkedList(2, 4, 3)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify the CtCI example`() {
        val list1 = createLinkedList(6, 1, 7)
        val list2 = createLinkedList(2, 9, 5)
        val expected = createLinkedList(9, 1, 2)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }
}
