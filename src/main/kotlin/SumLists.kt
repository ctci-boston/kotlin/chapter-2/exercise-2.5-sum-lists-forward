import com.pajato.ctci.Node

fun sumLists(list1: Node?, list2: Node?): Node? {
    tailrec fun getSum(powerOfTen: Int, left: Node?, right: Node?, leftSum: Int, rightSum: Int): Node? {
        fun getSum(node: Node, sum: Int): Int = (sum * 10) + node.value
        fun getStartPowerOfTen(powerOfTen: Int): Int =
            if (leftSum + rightSum > powerOfTen) powerOfTen else powerOfTen / 10
        tailrec fun buildOutput(powerOfTen: Int, sum: Int, head: Node?, current: Node?): Node? {
            if (powerOfTen == 0) return head
            val nextOutputDigit = sum / powerOfTen
            val nextCurrent = Node(nextOutputDigit)
            val nextSum = sum - (powerOfTen * nextOutputDigit)
            val nextHead = head ?: nextCurrent
            if (current != null) current.next = nextCurrent
            return buildOutput(powerOfTen / 10, nextSum, nextHead, nextCurrent)
        }

        if (left == null && right == null) return buildOutput(getStartPowerOfTen(powerOfTen), leftSum + rightSum, null, null)
        val nextLeftSum = if (left != null) getSum(left, leftSum) else leftSum
        val nextRightSum = if (right != null) getSum(right, rightSum) else rightSum
        return getSum(powerOfTen * 10, left?.next, right?.next, nextLeftSum, nextRightSum)
    }

    return when {
        list1 == null && list2 == null -> null
        else -> getSum(1, list1, list2, 0, 0)
    }
}
